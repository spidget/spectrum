#include<errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>

void write(unsigned char c, unsigned char *ck)
{
        putchar(c);
        *ck = *ck ^ c;
}

void lendian(unsigned short val, unsigned char *ck)
{
        write(val & 0xFF, ck);
        write(val >> 8 & 0xFF, ck);
}

int fsize(char *fname)
{
        struct stat s;

        if (stat(fname, &s) != 0) {
                fprintf(stderr,
                        "Stating error for %s: %s\n",
                        fname,
                        strerror(errno));
                exit(1);
        }

        return s.st_size;
}

void tap(char *in, unsigned short start)
{
        unsigned int i;
        unsigned char ck = 0;
        int bsize;
        FILE *f;

        /* all blocks start with length little endian, headers are 19 bytes */
        putchar(0x13);
        putchar(0x00);

        /* flag byte: 0 for header, 1 for data */
        write(0, &ck);

        /* data type, 3 is a byte header */
        write(3, &ck);

        /* name, 10 bytes space padded */
        write('B', &ck);
        write('e', &ck);
        write('e', &ck);
        write('p', &ck);
        write(' ', &ck);
        write('B', &ck);
        write('o', &ck);
        write('o', &ck);
        write('p', &ck);
        write('!', &ck);

        /* size of block we're writing */
        bsize = fsize(in);
        lendian(bsize, &ck);
        /* parameter 1: start of memory to write to for data blocks */
        lendian(start, &ck);
        /* parameter 2: data blocks are set to tihs. no idea why */
        lendian(32768, &ck);

        putchar(ck);

        /* block length = data length + flag + cksum */
        bsize = bsize + 2;
        putchar(bsize & 0xFF);
        putchar(bsize >> 8 & 0xFF);

        /* flag byte: 255 for data block */
        ck = 0;
        write(0xff, &ck);

        f = fopen(in, "rb");
        while ((i = fgetc(f)) != EOF)
                write(i, &ck);
        fclose(f);

        putchar(ck);
}

int main(int argc, char *argv[])
{
        tap(argv[1], atoi(argv[2]));
}
