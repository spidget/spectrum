        ; TODO: After the contentious 16k ram?
        org 0x8000

        DEVICE ZXSPECTRUM48
        SLDOPT COMMENT WPMEM, LOGPOINT, ASSERTION

READ_KEYBOARD equ 0xFE
SET_BORDER    equ 8859
SCREEN_SIZE   equ 6144
SCREEN        equ 0x4000
ATTRIB_SIZE   equ 0x300
ATTRIBUTE     equ 0x5800

BNM_DOT_SPACE equ %01111111
HJKL_ENTER    equ %10111111
YUIOP         equ %11011111
_67890        equ %11101111
_12345        equ %11110111
QWERT         equ %11111011
ASDFG         equ %11111101
SHIFT_ZXCV    equ %11111110

init:
        ; disable interrupts
        ; Stops ROM routines such as keyboard scanning. Should improve
        ; performance.
        di

        ; clear screen
        ld e, 0
        ld hl, SCREEN
        ld bc, SCREEN_SIZE
        call fill_memory

        ; initialise attributes
        ; no flash, bright, blue paper, white ink
        ld e, %00001111
        ld hl, ATTRIBUTE
        ld bc, 0x200
        call fill_memory

        ; initialise attributes
        ; no flash, bright, green paper, white ink
        ld e, %00100111
        ld hl, 0x5a00
        ld bc, 0x20
        call fill_memory

        ; initialise attributes
        ; no flash, no bright, green paper, white ink
        ld e, %01100111
        ld hl, 0x5a20
        ld bc, 0x180
        call fill_memory
main:
        ei
        halt
        di

        ; undraw last sprite
        ld hl, (wossname_y)
        ld bc, 8
        call clear_sprite

        call read_keyboard

        ld a, (input)
        bit 0, a
        call nz, move_right

        ld a, (input)
        bit 1, a
        call nz, move_left

        ld hl, wossname_left
        ld a, (input)
        bit 1, a
        jr nz, main_moving_left

        ld hl, wossname

main_moving_left:
        ld de, (wossname_frame)
        add hl, de
        ld de, hl

        ; also loads wossname_x
        ld hl, (wossname_y)
        ld bc, 8
        call draw_sprite

        jr main

read_keyboard:
        ; clear last recorded input
        ld a, 0
        ld (input), a

        ; read keys
        ld a, YUIOP
        in a, (READ_KEYBOARD)

        ; preserve the recorded input
        ld b, a

        ; test P
        bit 0, b
        jp nz, read_keyboard_no_p

        ; set moving right
        ld a, (input)
        set 0, a
        ld (input), a

read_keyboard_no_p:
        ; test O
        bit 1, b
        ret nz

        ; set moving left
        ld a, (input)
        set 1, a
        ld (input), a

        ret

move_left:
        ; move to previous frame
        ld a, (wossname_frame)

        sub 16
        ld (wossname_frame), a

        ; if we step beyond the first frame
        ret nc

        ; reset frame
        ld a, 0x70
        ld (wossname_frame), a

        ; just to make it visible
        call change_border

        ; move to previous cell
        ld a, (wossname_x)
        dec a
        ld (wossname_x), a

        ret

move_right:
        ; move to next frame
        ld a, (wossname_frame)
        add a, 16
        ld (wossname_frame), a

        ; got to the last frame?
        cp a, 0x80
        ret nz

        ; reset frame
        ld a, 0
        ld (wossname_frame), a

        ; just to make it visible
        call change_border

        ; move to next cell
        ld a, (wossname_x)
        inc a
        ld (wossname_x), a

        ret

; e  = data to fill
; hl = address to start
; bc = count
fill_memory:
        ld (hl), e; Fill byte
        inc hl; move to next byte
        dec bc; decrement counter

        ; check if bc = 0
        ld a, b
        or c

        jr nz, fill_memory
        ret

; h  = sprite x
; l  = sprite y
; bc = size of sprite in lines
clear_sprite:
        push hl
        push bc

        ld a, l
        call to_screen_pos

        ; load sprite x into bc
        ld b, 0
        ld c, h

        ; add sprite x to screen y
        add ix, bc

        pop bc
        push bc

        ld a, 0
        ld (ix), a
        ld (ix + 1), a

        pop bc
        pop hl

        ; next line
        inc l

        ; are we done?
        dec bc
        ld a, b
        or c

        jr nz, clear_sprite
        ret

; h  = sprite x
; l  = sprite y
; de = sprite address
; bc = size of sprite in lines
draw_sprite:
        push hl
        push bc

        ld a, l
        call to_screen_pos

        ld b, 0
        ld c, h

        add ix, bc

        pop bc
        push bc

        ld a, (de)
        ld (ix), a

        inc ix
        inc de
        ld a, (de)
        ld (ix), a

        inc de

        pop bc
        pop hl

        ; next line
        inc l

        ; are we done?
        dec bc
        ld a, b
        or c

        jr nz, draw_sprite
        ret

; l      = y pos
; ret ix = screen pos
to_screen_pos:
        push af
        push hl
        push bc

        ld ix, screen_line_lookup

        ; copy y into bc
        ld c, l
        ld b, 0

        ; double since each record in lookup table is 2 bytes
        sla bc

        ; point to the record for the line
        add ix, bc

        ; perform the lookup
        ld hl, (ix)
        ld ix, hl

        pop bc
        pop hl
        pop af

        ret

change_border:
        push af

        ld a, (border_colour)
        inc a
        cp a, 7 ; 7=white, don't want a white border
        jp nz, change_border_end
        ld a, 0 ; reset to black

change_border_end:
        ld (border_colour), a
        call SET_BORDER

        pop af
        ret

screen_buffer:
        defs SCREEN_SIZE, 0

border_colour:
        defb 0

wossname_frame:
        defw 0

wossname_y:
        defb 120

wossname_x:
        defb 0

input:
        defb 0

screen_line_lookup:
        defw 0x4000
        defw 0x4100
        defw 0x4200
        defw 0x4300
        defw 0x4400
        defw 0x4500
        defw 0x4600
        defw 0x4700
        defw 0x4020
        defw 0x4120
        defw 0x4220
        defw 0x4320
        defw 0x4420
        defw 0x4520
        defw 0x4620
        defw 0x4720
        defw 0x4040
        defw 0x4140
        defw 0x4240
        defw 0x4340
        defw 0x4440
        defw 0x4540
        defw 0x4640
        defw 0x4740
        defw 0x4060
        defw 0x4160
        defw 0x4260
        defw 0x4360
        defw 0x4460
        defw 0x4560
        defw 0x4660
        defw 0x4760
        defw 0x4080
        defw 0x4180
        defw 0x4280
        defw 0x4380
        defw 0x4480
        defw 0x4580
        defw 0x4680
        defw 0x4780
        defw 0x40A0
        defw 0x41A0
        defw 0x42A0
        defw 0x43A0
        defw 0x44A0
        defw 0x45A0
        defw 0x46A0
        defw 0x47A0
        defw 0x40C0
        defw 0x41C0
        defw 0x42C0
        defw 0x43C0
        defw 0x44C0
        defw 0x45C0
        defw 0x46C0
        defw 0x47C0
        defw 0x40E0
        defw 0x41E0
        defw 0x42E0
        defw 0x43E0
        defw 0x44E0
        defw 0x45E0
        defw 0x46E0
        defw 0x47E0
        defw 0x4800
        defw 0x4900
        defw 0x4A00
        defw 0x4B00
        defw 0x4C00
        defw 0x4D00
        defw 0x4E00
        defw 0x4F00
        defw 0x4820
        defw 0x4920
        defw 0x4A20
        defw 0x4B20
        defw 0x4C20
        defw 0x4D20
        defw 0x4E20
        defw 0x4F20
        defw 0x4840
        defw 0x4940
        defw 0x4A40
        defw 0x4B40
        defw 0x4C40
        defw 0x4D40
        defw 0x4E40
        defw 0x4F40
        defw 0x4860
        defw 0x4960
        defw 0x4A60
        defw 0x4B60
        defw 0x4C60
        defw 0x4D60
        defw 0x4E60
        defw 0x4F60
        defw 0x4880
        defw 0x4980
        defw 0x4A80
        defw 0x4B80
        defw 0x4C80
        defw 0x4D80
        defw 0x4E80
        defw 0x4F80
        defw 0x48A0
        defw 0x49A0
        defw 0x4AA0
        defw 0x4BA0
        defw 0x4CA0
        defw 0x4DA0
        defw 0x4EA0
        defw 0x4FA0
        defw 0x48C0
        defw 0x49C0
        defw 0x4AC0
        defw 0x4BC0
        defw 0x4CC0
        defw 0x4DC0
        defw 0x4EC0
        defw 0x4FC0
        defw 0x48E0
        defw 0x49E0
        defw 0x4AE0
        defw 0x4BE0
        defw 0x4CE0
        defw 0x4DE0
        defw 0x4EE0
        defw 0x4FE0
        defw 0x5000
        defw 0x5100
        defw 0x5200
        defw 0x5300
        defw 0x5400
        defw 0x5500
        defw 0x5600
        defw 0x5700
        defw 0x5020
        defw 0x5120
        defw 0x5220
        defw 0x5320
        defw 0x5420
        defw 0x5520
        defw 0x5620
        defw 0x5720
        defw 0x5040
        defw 0x5140
        defw 0x5240
        defw 0x5340
        defw 0x5440
        defw 0x5540
        defw 0x5640
        defw 0x5740
        defw 0x5060
        defw 0x5160
        defw 0x5260
        defw 0x5360
        defw 0x5460
        defw 0x5560
        defw 0x5660
        defw 0x5760
        defw 0x5080
        defw 0x5180
        defw 0x5280
        defw 0x5380
        defw 0x5480
        defw 0x5580
        defw 0x5680
        defw 0x5780
        defw 0x50A0
        defw 0x51A0
        defw 0x52A0
        defw 0x53A0
        defw 0x54A0
        defw 0x55A0
        defw 0x56A0
        defw 0x57A0
        defw 0x50C0
        defw 0x51C0
        defw 0x52C0
        defw 0x53C0
        defw 0x54C0
        defw 0x55C0
        defw 0x56C0
        defw 0x57C0
        defw 0x50E0
        defw 0x51E0
        defw 0x52E0
        defw 0x53E0
        defw 0x54E0
        defw 0x55E0
        defw 0x56E0
        defw 0x57E0

wossname:
        defb %00111000, %00000000
        defb %00111100, %00000000
        defb %00011000, %00000000
        defb %00011001, %00000000
        defb %01111110, %00000000
        defb %00011000, %00000000
        defb %01110100, %00000000
        defb %00000010, %00000000

; frame 2
        defb %00011100, %00000000
        defb %00011110, %00000000
        defb %00001100, %00000000
        defb %00001101, %00000000
        defb %00011110, %00000000
        defb %00001100, %00000000
        defb %00111010, %00000000
        defb %00000010, %00000000

; frame 3
        defb %00001110, %00000000
        defb %00001111, %00000000
        defb %00000110, %00000000
        defb %00000110, %10000000
        defb %00001111, %00000000
        defb %00000110, %00000000
        defb %00011110, %00000000
        defb %00000010, %00000000

; frame 4
        defb %00000111, %00000000
        defb %00000111, %10000000
        defb %00000011, %00000000
        defb %00000011, %10000000
        defb %00000111, %10000000
        defb %00000011, %00000000
        defb %00000011, %00000000
        defb %00000001, %00000000

; frame 5
        defb %00000111, %00000000
        defb %00000111, %10000000
        defb %00000011, %00000000
        defb %00000111, %00000000
        defb %00000111, %10000000
        defb %00000011, %00000000
        defb %00000001, %00000000
        defb %00000010, %00000000

; frame 6
        defb %00000011, %10000000
        defb %00000011, %11000000
        defb %00000001, %10000000
        defb %00000011, %11000000
        defb %00000111, %11000000
        defb %00000001, %10000000
        defb %00000010, %10000000
        defb %00000100, %00000000

; frame 7
        defb %00000000, %11100000
        defb %00000000, %11110000
        defb %00000000, %01100000
        defb %00000000, %01101000
        defb %00000001, %11110000
        defb %00000000, %01100000
        defb %00000001, %11010000
        defb %00000000, %00010000

; frame 8
        defb %00000000, %11100000
        defb %00000000, %11110000
        defb %00000000, %01100000
        defb %00000000, %01100100
        defb %00000001, %11111000
        defb %00000000, %01100000
        defb %00000001, %11010000
        defb %00000000, %00001000

wossname_left:
	defb %00000111, %00000000
	defb %00001111, %00000000
	defb %00000110, %00000000
	defb %00100110, %00000000
	defb %00011111, %10000000
	defb %00000110, %00000000
	defb %00001011, %10000000
	defb %00010000, %00000000

; frame 2
	defb %00000111, %00000000
	defb %00001111, %00000000
	defb %00000110, %00000000
	defb %00010110, %00000000
	defb %00001111, %10000000
	defb %00000110, %00000000
	defb %00001011, %10000000
	defb %00001000, %00000000
; frame 3
	defb %00000001, %11000000
	defb %00000011, %11000000
	defb %00000001, %10000000
	defb %00000011, %11000000
	defb %00000011, %11100000
	defb %00000001, %10000000
	defb %00000001, %01000000
	defb %00000000, %00100000

; frame 4
	defb %00000000, %11100000
	defb %00000001, %11100000
	defb %00000000, %11000000
	defb %00000000, %11100000
	defb %00000001, %11100000
	defb %00000000, %11000000
	defb %00000000, %10000000
	defb %00000000, %01000000

; frame 5
	defb %00000000, %11100000
	defb %00000001, %11100000
	defb %00000000, %11000000
	defb %00000001, %11000000
	defb %00000001, %11100000
	defb %00000000, %11000000
	defb %00000000, %11000000
	defb %00000000, %10000000

; frame 6
	defb %00000000, %01110000
	defb %00000000, %11110000
	defb %00000000, %01100000
	defb %00000001, %01100000
	defb %00000000, %11110000
	defb %00000000, %01100000
	defb %00000000, %01111000
	defb %00000000, %01000000

; frame 7
	defb %00000000, %00111000
	defb %00000000, %01111000
	defb %00000000, %00110000
	defb %00000000, %10110000
	defb %00000000, %01111000
	defb %00000000, %00110000
	defb %00000000, %01011100
	defb %00000000, %01000000

; frame 8
	defb %00000000, %00011100
	defb %00000000, %00111100
	defb %00000000, %00011000
	defb %00000000, %10011000
	defb %00000000, %01111110
	defb %00000000, %00011000
	defb %00000000, %00101110
	defb %00000000, %01000000

        SAVESNA "code.sna", init