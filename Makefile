CFLAGS=-Wall -g

all: fin.tap

run-quick: fin-quick.tap
	fuse-gtk -d default fin-quick.tap

.PHONY: all clean run run-quick

fin.tap: scr.tap loader.tap code.tap
	cat loader.tap scr.tap code.tap > $@

fin-quick.tap: loader-quick.tap code.tap
	cat loader-quick.tap code.tap > $@

mkscr: mkscr.o

%.bin: %.asm
	sjasmplus $< --fullpath --sld="$*.sld" --raw="$@"

loader.tap: loader.bas Makefile
	zmakebas -a 10 -n "Stuff" -o $@ $<

loader-quick.tap: loader-quick.bas Makefile
	zmakebas -a 10 -n "Stuff" -o $@ $<

scr.tap: loader.scr mkscr
	./mkscr loader.scr 16384 > scr.tap

code.tap: code.bin mkscr
	./mkscr code.bin 32768 > code.tap

clean:
	rm -f *.tap *.o mkscr *.bin *.sld *.sna

run: fin.tap
	fuse-gtk --no-accelerate-loader -d default fin.tap
